import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Navigator from "./src/navigator";
import { Provider } from "react-redux";
import store from "./src/redux/configureStore";

import { decode, encode } from "base-64";

import { YellowBox } from "react-native";
// ignore warning due to firestore and expo #ref: https://github.com/firebase/firebase-js-sdk/issues/97
YellowBox.ignoreWarnings(["Setting a timer"]);

// const [currentUser, setCurrentUser] = useState(() =>
//     getDbUser(res => {
//         setCurrentUser(res);
//     })
// );

// TODO : useEffect -> verifier auth avec async storage

if (!global.btoa) {
    global.btoa = encode;
}

if (!global.atob) {
    global.atob = decode;
}

export default function App() {
    return (
        <Provider store={store}>
            <Navigator />
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    }
});
