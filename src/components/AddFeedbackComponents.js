import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Button, ThemeProvider, Text, Input } from "react-native-elements";
import { Feather } from "@expo/vector-icons";
import ErrorComponent from "./ErrorComponent";
import MessageComponent from "./MessageComponent";
import useFeedbacks from "../modules/useFeedbacks";
import { useSelector } from "react-redux";

const AddFeedbackComponents = () => {
    const [content, setContent] = useState("");
    const { addFeedback, errorMessageFeedback, message } = useFeedbacks();

    const user = useSelector(state => state.user);

    return (
        <ThemeProvider>
            <View style={styles.section}>
                {errorMessageFeedback ? (
                    <ErrorComponent message={errorMessageFeedback} />
                ) : null}
                {message ? <MessageComponent message={message} /> : null}

                <Text style={styles.text}> Write a new feedback here : </Text>
                <Input
                    autoCapitalize="none"
                    placeholder="Content"
                    onChangeText={setContent}
                    value={content}
                />
                <View style={styles.button}>
                    <Button
                        type="outline"
                        title="  Send Feedback"
                        icon={<Feather name="send" color="#0c71c3" size={15} />}
                        onPress={() => {
                            addFeedback(user.user.token, content);
                            setContent("");
                        }}
                    />
                </View>
            </View>
        </ThemeProvider>
    );
};

const styles = StyleSheet.create({
    text: {
        marginBottom: 10,
        fontSize: 18,
        color: "#0c71c3"
    },
    section: {
        padding: 10,
        backgroundColor: "rgb(250,250,250)",
        borderRadius: 12,
        marginVertical: 10
    },
    button: {
        marginHorizontal: 50,
        marginVertical: 10
        //color: "#0c71c3"
    }
});

export default AddFeedbackComponents;
