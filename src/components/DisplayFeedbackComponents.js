import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Button, ThemeProvider, Text, ListItem } from "react-native-elements";

import { ScrollView } from "react-native-gesture-handler";
import FeedbackDetailsComponents from "./FeedbackDetailsComponents";
import { useSelector } from "react-redux";

import useFeedbacks from "../modules/useFeedbacks";

const DisplayFeedbackComponents = ({ currentUser }) => {
    const { get, errorMessageFeedback, feedbacks } = useFeedbacks();

    const user = useSelector(state => state.user);

    useEffect(() => {
        get(user.user.token);
    }, [user]);

    return (
        <>
            {errorMessageFeedback ? (
                <ErrorComponent message={errorMessageFeedback} />
            ) : null}
            <Text style={styles.text}> Your feedbacks : </Text>
            <View style={{ height: 200 }}>
                <FlatList
                    style={{ flex: 1 }}
                    data={feedbacks}
                    renderItem={({ item }) => {
                        return <FeedbackDetailsComponents item={item} />;
                    }}
                />
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    text: {
        marginBottom: 10,
        fontSize: 18,
        color: "#0c71c3"
    }
});

export default DisplayFeedbackComponents;
