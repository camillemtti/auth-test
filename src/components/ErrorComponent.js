import React, { useEffect, useState } from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";
import firebase from "../modules/firebase";
import { Button, ThemeProvider, Text, Input } from "react-native-elements";
import { AntDesign } from "@expo/vector-icons";

const ErrorComponent = ({ message }) => {
    return (
        <View style={styles.error}>
            <AntDesign
                name="warning"
                style={{ color: "red", marginHorizontal: 5 }}
                size={15}
            />
            <Text style={{ color: "red" }}>{message}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    error: {
        color: "red",
        borderColor: "red",
        borderWidth: 1,
        borderRadius: 5,
        padding: 15,
        flexDirection: "row"
    }
});

export default ErrorComponent;
