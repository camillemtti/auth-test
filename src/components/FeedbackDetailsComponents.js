import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, ThemeProvider, Text, ListItem } from "react-native-elements";

const FeedbackDetailsComponents = ({ item }) => {
    var a = new Date(item.date);
    var year = a.getFullYear();
    var month = a.getMonth() + 1;
    var date = a.getDate();

    return (
        <View>
            <ListItem
                title={item.content}
                subtitle={
                    <View style={styles.subtitleView}>
                        <Text style={styles.ratingText}>
                            Sent :{date}/{month}/{year}
                        </Text>
                    </View>
                }
                bottomDivider
            />
        </View>
    );
};

const styles = StyleSheet.create({});

export default FeedbackDetailsComponents;
