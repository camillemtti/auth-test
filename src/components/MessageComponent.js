import React, { useEffect, useState } from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";
import firebase from "../modules/firebase";
import { Button, ThemeProvider, Text, Input } from "react-native-elements";
import { AntDesign } from "@expo/vector-icons";

const MessageComponent = ({ message }) => {
    return (
        <View style={styles.error}>
            <AntDesign
                name="checkcircleo"
                style={{ color: "green", marginHorizontal: 5 }}
                size={15}
            />
            <Text style={{ color: "green" }}>{message}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    error: {
        color: "green",
        borderColor: "green",
        borderWidth: 1,
        borderRadius: 5,
        padding: 15,
        flexDirection: "row"
    }
});

export default MessageComponent;
