import axios from "axios";

export default axios.create({
    baseURL: "https://stable-electron-271313.appspot.com"
});
