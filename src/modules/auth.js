import firebase from "./firebase";
import { useState, useEffect } from "react";
import db from "./database";
import { useSelector, useDispatch } from "react-redux";
import { addUser } from "../redux/user/userReducers";
import api from "./api";
import { app } from "firebase";

export default () => {
    const [errorMessage, setErrorMessage] = useState(null);

    const dispatch = useDispatch();

    const handleLogin = (email, password, callback) => {
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                const userAuth = firebase.auth().currentUser;

                setErrorMessage(null);

                firebase
                    .auth()
                    .currentUser.getIdToken(/* forceRefresh */ true)
                    .then(function(idToken) {
                        api.get("/me", {
                            headers: { Authorization: idToken }
                        }).then(user => {
                            dispatch(
                                addUser(
                                    userAuth.uid,
                                    idToken,
                                    user.data.first_name,
                                    user.data.last_name,
                                    user.data.role
                                )
                            );
                        });
                    });

                if (callback) {
                    callback();
                }
            })
            .catch(error => {
                setErrorMessage(error.toString());
            });
    };

    const handleSignUp = (
        email,
        password,
        firstname,
        lastname,
        role,
        callback
    ) => {
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
                setErrorMessage(null);
                const myUser = firebase.auth().currentUser;
                const data = {
                    id: myUser.uid,
                    admin: role,
                    first_name: firstname,
                    last_name: lastname
                };
                api.post("/users", data).then(() => {
                    const userAuth = firebase.auth().currentUser;

                    firebase
                        .auth()
                        .currentUser.getIdToken(/* forceRefresh */ true)
                        .then(function(idToken) {
                            api.get("/me", {
                                headers: { Authorization: idToken }
                            }).then(user => {
                                dispatch(
                                    addUser(
                                        userAuth.uid,
                                        idToken,
                                        user.data.first_name,
                                        user.data.last_name,
                                        user.data.role
                                    )
                                );
                            });
                        });

                    if (callback) {
                        callback();
                    }
                });
            })
            .catch(error => {
                setErrorMessage(error.toString());
            });
    };

    const handleLogOut = callback => {
        firebase
            .auth()
            .signOut()
            .then(() => {
                if (callback) {
                    callback();
                }
            })
            .catch(error => setErrorMessage(error.toString()));
    };

    const getDbUser = async cb => {
        const userAuth = await firebase.auth().currentUser;
        // if no user is retrieved there is an error
        if (userAuth === null) {
            setErrorMessage("user not authenticated");
        } else {
            // try to find user in users collection in firestore
            const user = api.get("/me");
            const docRef = db.collection("users").doc(userAuth.uid);
            let data = null;
            await docRef
                .get()
                .then(doc => {
                    //when request is finished
                    if (doc.exists) {
                        data = doc.data();
                        setErrorMessage(null);
                    } else {
                        setErrorMessage("User not found");
                    }
                    cb({ id: userAuth.uid, ...data });
                })
                .catch(error => {
                    setErrorMessage(error.toString());
                    cb(data);
                });

            return data;
        }
    };

    const getToken = () => {
        firebase
            .auth()
            .currentUser.getIdToken(/* forceRefresh */ true)
            .then(function(idToken) {})
            .catch(function(error) {
                // Handle error
            });
    };

    return {
        errorMessage,
        handleLogin,
        handleSignUp,
        handleLogOut,
        getDbUser,
        getToken
    };
};
