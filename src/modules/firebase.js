import * as firebase from "firebase";
import "firebase/firestore";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyAceW6rIhWWRyQWsYrBXfbCW1-Z7CfepXI",
    authDomain: "test-10971.firebaseapp.com",
    databaseURL: "https://test-10971.firebaseio.com",
    storageBucket: "test-10971.appspot.com",
    appId: "test-10971",
    projectId: "test-10971"
};

try {
    firebase.initializeApp(firebaseConfig);
} catch {}

export default firebase;
