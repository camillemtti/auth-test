import db from "./database";
import { useState, useEffect } from "react";
import api from "./api";
import { useSelector } from "react-redux";

export default () => {
    const [errorMessageFeedback, setErrorMessageFeedback] = useState(null);
    const [message, setMessage] = useState(null);
    const [feedbacks, setFeedbacks] = useState([]);

    const addFeedback = (token, content) => {
        api.post(
            "/feedbacks",

            {
                content
            },
            {
                headers: { Authorization: token }
            }
        )
            .then(() => {
                setMessage("Feedback sent");
                setErrorMessageFeedback(null);
                get(token);
            })
            .catch(err => {
                setErrorMessageFeedback(err.toString());
                setMessage(null);
            });
    };

    const get = token => {
        api.get("/feedbacks", {
            headers: { Authorization: token }
        }).then(resp => {
            if (resp.data.empty) {
                return setErrorMessageFeedback("No matching documents.");
            }
            setFeedbacks(resp.data);
        });
    };

    return {
        feedbacks,
        get,
        errorMessageFeedback,
        addFeedback
    };
};
