import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import SignUp from "./screens/SignUp";
import Landing from "./screens/Landing";
import HomeScreen from "./screens/HomeScreen";
import TestReduxScreen from "./screens/TestReduxScreen";
import { useEffect } from "react";

const Navigator = createStackNavigator(
    {
        SignUp,
        Landing,
        HomeScreen,
        TestReduxScreen
    },
    {
        initialRouteName: "Landing",
        defaultNavigationOptions: {
            title: "Feedback apps"
        }
    }
);

export default createAppContainer(Navigator);
