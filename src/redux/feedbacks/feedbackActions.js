const addFeedback = feedback => {
    return {
        type: "ADD_FEEDBACK",
        payload: feedback
    };
};

const updateFeedback = feedback => {
    return {
        type: "UPDATE_FEEDBACK",
        payload: feedback
    };
};

const deleteFeedback = id => {
    return {
        type: "DELETE_FEEDBACK",
        payload: id
    };
};

export default { addFeedback, updateFeedback, deleteFeedback };
