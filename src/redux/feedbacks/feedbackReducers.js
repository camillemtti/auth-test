const initialState = [];

const feedbackReducer = (state = initialState, action) => {
    switch (action.type) {
        case "DELETE_FEEDBACK":
            return state.filter(feedback => feedback.id !== action.payload);

        case "ADD_FEEDBACK":
            return [
                ...state,
                {
                    content: action.payload.content,
                    from: action.payload.from,
                    date: Date.now()
                }
            ];

        case "UPDATE_FEEDBACK":
            return state.map(feedback => {
                return feedback.id === action.payload.id
                    ? {
                          content: action.payload.content,
                          from: action.payload.from,
                          date: Date.now()
                      }
                    : feedback;
            });

        default:
            return state;
    }
};

export default feedbackReducer;
