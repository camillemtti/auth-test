const initialState = {
    list: [
        {
            content: "bonjour quentin",
            id: 1
        }
    ]
};
const ADD_TEST = "ADD_TEST";

export const addTest = (content, id) => {
    return {
        type: ADD_TEST,
        payload: { content, id }
    };
};

const updateTest = test => {
    return {
        type: "UPDATE_TEST",
        payload: test
    };
};

const deleteTest = id => {
    return {
        type: "DELETE_TEST",
        payload: id
    };
};

const testReducer = (state = initialState, action) => {
    switch (action.type) {
        case "DELETE_TEST":
            return state.filter(test => test.id !== action.payload);

        case ADD_TEST:
            return {
                ...state,
                list: [
                    ...state.list,

                    {
                        content: action.payload.content,
                        id: action.payload.id
                    }
                ]
            };

        case "UPDATE_TEST":
            return state.map(test => {
                return test.id === action.payload.id
                    ? {
                          content: action.payload.content
                      }
                    : test;
            });

        default:
            return state;
    }
};

export default testReducer;
