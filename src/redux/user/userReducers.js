const initialState = { user: {} };
const ADD_USER = "ADD_USER";

export const addUser = (id, token, first_name, last_name, role) => {
    return {
        type: ADD_USER,
        payload: { id, token, first_name, last_name, role }
    };
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_USER:
            const role = action.payload.role === 0 ? "user" : "admin";
            return {
                user: {
                    id: action.payload.id,
                    token: action.payload.token,
                    first_name: action.payload.first_name,
                    last_name: action.payload.last_name,
                    role
                }
            };

        default:
            return state;
    }
};

export default userReducer;
