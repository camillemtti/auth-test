import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Button, ThemeProvider, Text, Image } from "react-native-elements";
import auth from "../modules/auth";
import ErrorComponent from "../components/ErrorComponent";
import AddFeedbackComponents from "../components/AddFeedbackComponents";
import DisplayFeedbackComponents from "../components/DisplayFeedbackComponents";

import { YellowBox } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import { useSelector, useDispatch } from "react-redux";
// ignore warning due to firestore and expo #ref: https://github.com/firebase/firebase-js-sdk/issues/97
YellowBox.ignoreWarnings(["Setting a timer"]);

const HomeScreen = ({ navigation }) => {
    const { handleLogOut, getDbUser, errorMessage } = auth();
    const currentUser = useSelector(state => state.user.user);

    // ---- Auth stuff ---
    // const [currentUser, setCurrentUser] = useState(() =>
    //     getDbUser(res => {
    //         setCurrentUser(res);
    //     })
    // );

    //console.log(currentUser);

    if (errorMessage) {
        navigation.replace("Landing");
    }

    // ---- End of auth stuff ---

    const callback = () => {
        navigation.replace("Landing");
    };

    return (
        <ThemeProvider>
            <View style={styles.container}>
                {errorMessage ? (
                    <ErrorComponent message={errorMessage} />
                ) : null}
                <View style={styles.section}>
                    <Image
                        source={require("../../assets/img/feedback.png")}
                        style={{ width: 180, height: 180 }}
                    />
                    <Text style={styles.text}>
                        Welcome {currentUser.first_name}! You are a{" "}
                        {currentUser.role}
                    </Text>
                </View>

                <View>
                    <AddFeedbackComponents />

                    <DisplayFeedbackComponents />
                </View>
                {/* 
                {currentUser.role === "Sender" ? (
                    <AddFeedbackComponents currentUser={currentUser} />
                ) : null}

                {currentUser.role === "Receiver" ? (
                    <DisplayFeedbackComponents />
                ) : null} */}

                <View style={styles.button}>
                    <Button
                        type="outline"
                        title="Logout"
                        onPress={() => {
                            handleLogOut(callback);
                        }}
                    />
                </View>
            </View>
        </ThemeProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    section: {
        padding: 10,
        alignItems: "center",
        borderRadius: 10,
        marginVertical: 10
    },
    button: {
        marginHorizontal: 50,
        marginVertical: 10
        //color: "#0c71c3"
    },
    text: {
        marginVertical: 10,
        fontSize: 18
    }
});

export default HomeScreen;
