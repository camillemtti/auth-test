import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Button, ThemeProvider, Text, Input } from "react-native-elements";
import ErrorComponent from "../components/ErrorComponent";
import auth from "../modules/auth";

const Landing = ({ navigation }) => {
    const [email, setEmail] = useState("user@yopmail.com");
    const [password, setPassword] = useState("test123");
    const { errorMessage, handleLogin } = auth();

    const callback = () => {
        navigation.replace("HomeScreen");
    };

    // useEffect(() => {
    //     getPushNotificationPermissions();
    // }, []);

    return (
        <ThemeProvider>
            <View style={styles.container}>
                <View style={{ alignItems: "center" }}>
                    <Text h3>Landing Screen</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.text}>
                        You already have an account ?
                    </Text>
                    {errorMessage ? (
                        <ErrorComponent message={errorMessage} />
                    ) : null}
                    <View style={styles.form}>
                        <Input
                            autoCapitalize="none"
                            placeholder="Email"
                            onChangeText={setEmail}
                            value={email}
                        />
                        <Input
                            secureTextEntry
                            autoCapitalize="none"
                            placeholder="Password"
                            onChangeText={setPassword}
                            value={password}
                        />
                    </View>
                    <View style={styles.button}>
                        <Button
                            type="outline"
                            title="Log in"
                            onPress={() =>
                                handleLogin(email, password, callback)
                            }
                        />
                    </View>
                </View>

                <View style={{ alignItems: "center" }}>
                    <Text>Or else</Text>
                    <Button
                        type="clear"
                        title="Sign Up"
                        onPress={() => navigation.navigate("SignUp")}
                    />
                </View>
            </View>
        </ThemeProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    section: {
        padding: 10,
        backgroundColor: "rgb(250,250,250)",
        borderRadius: 10,
        marginVertical: 10
    },
    button: {
        marginHorizontal: 50,
        marginVertical: 10

        //color: "#0c71c3"
    },
    text: {
        marginBottom: 10,
        fontSize: 18,
        color: "#0c71c3"
    },
    form: {
        marginBottom: 10
    }
});

export default Landing;
