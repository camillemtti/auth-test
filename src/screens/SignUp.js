import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import {
    Button,
    ThemeProvider,
    Text,
    Input,
    ButtonGroup
} from "react-native-elements";
import auth from "../modules/auth";
import ErrorComponent from "../components/ErrorComponent";

const SignUp = ({ navigation }) => {
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [role, setRole] = useState(0);
    const [password, setPassword] = useState("");
    const { errorMessage, handleSignUp } = auth();

    const buttons = ["User", "Admin"];

    const callback = () => {
        navigation.replace("HomeScreen");
    };

    return (
        <ThemeProvider>
            <View style={styles.container}>
                <View style={{ alignItems: "center" }}>
                    <Text h3>Sign up </Text>
                </View>
                <View style={styles.section}>
                    {errorMessage ? (
                        <ErrorComponent message={errorMessage} />
                    ) : null}
                    <View style={styles.form}>
                        <View style={styles.form}>
                            <Text style={styles.text}>
                                Personnal information :
                            </Text>
                            <Input
                                placeholder="First Name"
                                autoCapitalize="none"
                                style={{ flex: 1 }}
                                onChangeText={setFirstName}
                                value={firstName}
                            />
                            <Input
                                placeholder="Last Name"
                                autoCapitalize="none"
                                style={{ flex: 1 }}
                                onChangeText={setLastName}
                                value={lastName}
                            />
                        </View>
                        <Text style={styles.text}>Role : </Text>
                        <ButtonGroup
                            selectedIndex={role}
                            onPress={selectedIndex => setRole(selectedIndex)}
                            buttons={["User", "Admin"]}
                            containerStyle={{ height: 40 }}
                        />

                        <Text style={styles.text}>
                            Authentication information :{" "}
                        </Text>
                        <Input
                            placeholder="Email"
                            autoCapitalize="none"
                            style={styles.textInput}
                            onChangeText={setEmail}
                            value={email}
                        />
                        <Input
                            secureTextEntry
                            placeholder="Password"
                            autoCapitalize="none"
                            style={styles.textInput}
                            onChangeText={setPassword}
                            value={password}
                        />
                    </View>
                    <View style={styles.button}>
                        <Button
                            type="outline"
                            title="Sign Up"
                            onPress={() =>
                                handleSignUp(
                                    email,
                                    password,
                                    firstName,
                                    lastName,
                                    role,
                                    callback
                                )
                            }
                        />
                    </View>
                </View>

                <View style={{ alignItems: "center" }}>
                    <Text>Already have an account? </Text>
                    <Button
                        type="clear"
                        title="Log in"
                        onPress={() => navigation.navigate("Landing")}
                    />
                </View>
            </View>
        </ThemeProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    section: {
        padding: 10,
        backgroundColor: "rgb(250,250,250)",
        borderRadius: 10,
        marginVertical: 10
    },
    button: {
        marginHorizontal: 50,
        marginVertical: 10
        //color: "#0c71c3"
    },
    text: {
        marginBottom: 10,
        fontSize: 18,
        color: "#0c71c3"
    },
    form: {
        marginBottom: 10
    }
});

export default SignUp;
