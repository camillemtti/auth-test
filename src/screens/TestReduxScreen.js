import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Button, ThemeProvider, Text, Image } from "react-native-elements";
import auth from "../modules/auth";
import ErrorComponent from "../components/ErrorComponent";
import AddFeedbackComponents from "../components/AddFeedbackComponents";
import DisplayFeedbackComponents from "../components/DisplayFeedbackComponents";
import { addTest } from "../redux/test/testReducers.js";

import { YellowBox } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useSelector, useDispatch } from "react-redux";
// ignore warning due to firestore and expo #ref: https://github.com/firebase/firebase-js-sdk/issues/97
YellowBox.ignoreWarnings(["Setting a timer"]);

const TestReduxScreen = ({ navigation }) => {
    const salut = useSelector(state => state.test);
    console.log(salut);
    const dispatch = useDispatch();
    return (
        <ThemeProvider>
            <Text>Salu</Text>
            <Button
                type="outline"
                title="ADD Test"
                onPress={() => dispatch(addTest("au revoir", 3))}
            />
        </ThemeProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    section: {
        padding: 10,
        alignItems: "center",
        borderRadius: 10,
        marginVertical: 10
    },
    button: {
        marginHorizontal: 50,
        marginVertical: 10,
        color: "#0c71c3"
    },
    text: {
        marginVertical: 10,
        fontSize: 18
    }
});

export default TestReduxScreen;
